import { DobedoClientPage } from './app.po';

describe('dobedo-client App', function() {
  let page: DobedoClientPage;

  beforeEach(() => {
    page = new DobedoClientPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});

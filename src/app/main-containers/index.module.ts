import { NgModule } from '@angular/core';

import { NavbarComponent } from './navbar/navbar.component';
import { MainContainerRoutingModule } from './index-routing.module';
import { MainContainersComponent } from './main-containers.component';

@NgModule({
    imports: [
        MainContainerRoutingModule
    ],
    // declarations: [LayoutComponent, SidebarComponent, HeaderComponent]
    declarations: [MainContainersComponent, NavbarComponent]
})
export class MainContainerModule {}
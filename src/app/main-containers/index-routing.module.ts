import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainContainersComponent } from './main-containers.component';

const routes: Routes = [
    {
        path: '',
        component: MainContainersComponent,
        children: [
            { path: '', redirectTo: 'aboutus', pathMatch: 'prefix' },
            { path: 'aboutus', loadChildren: './view-aboutus/aboutus.module#AboutusModule' },
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class MainContainerRoutingModule {}
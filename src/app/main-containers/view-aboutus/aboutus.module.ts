import { NgModule } from '@angular/core';

import { AboutusRoutingModule } from './aboutus-routing.module';
import { AboutusComponent } from './aboutus.component';

@NgModule({
    imports: [
        AboutusRoutingModule,
    ],
    declarations: [
        AboutusComponent,
    ]
})
export class AboutusModule {}
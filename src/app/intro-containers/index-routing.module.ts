import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewIntroComponent } from './view-intro/view-intro.component';
const routes: Routes = [
    {
        path: '',
        component: ViewIntroComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class IntroRoutingModule {}
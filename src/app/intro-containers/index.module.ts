import { NgModule } from '@angular/core';

import { IntroRoutingModule } from './index-routing.module';
import { ViewIntroComponent } from './view-intro/view-intro.component';

@NgModule({
    imports: [IntroRoutingModule],
    declarations: [ViewIntroComponent]
})
export class IntroModule {}
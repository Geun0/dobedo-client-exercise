import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ViewIntroComponent } from './view-intro.component'

@NgModule({
    imports: [
        RouterModule
    ],
    declarations: [
        ViewIntroComponent
    ],
    exports: [
        ViewIntroComponent
    ]
})

export class IntroModule { }
import { Component, OnInit } from '@angular/core';
// import { routerTransition } from '../../router.animations';
import { animate, state, style, transition, trigger, keyframes } from '@angular/animations';
// import animations from '../../router.animations';

@Component({
  selector: 'app-view-intro',
  templateUrl: './view-intro.component.html',
  styleUrls: ['./view-intro.component.scss'],
  // animations: [routerTransition()]
  // animations: [
  //   trigger('routerTransition', [
  //     state('enter', style({
  //       transform: 'translateY(100%)'
  //     })),
  //     transition('enter => enter',[
  //       animate('0.5s ease-in-out', style({ transform: 'translateY(0%)' }))
  //     ]),
  //   ])
  // ]
  // animations: [
  //   trigger('movePanel', [
  //     state('inactive', style({
  //       opacity: 1,
  //       transform: 'translateY(0)',
  //       offset: 1
  //     })),
  //     transition('active => inactive', [
  //       animate(600, keyframes([
  //         style({ opacity: 0, transform: 'translateY(-200px)', offset: 0 }),
  //         style({ opacity: 1, transform: 'translateY(25px)', offset: .75 }),
  //         style({ opacity: 1, transform: 'translateY(0)', offset: 1 }),
  //       ]))
  //     ])

  //   ])
  // ]
})
export class ViewIntroComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}

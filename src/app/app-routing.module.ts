import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

const routes: Routes = [
    { path: '', loadChildren: './intro-containers/index.module#IntroModule'},
    { path: 'intro', loadChildren: './intro-containers/index.module#IntroModule'},
    { path: 'main', loadChildren: './main-containers/index.module#MainContainerModule' },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
